import React from 'react';

const HolaMundo = () => {
    const Hello ="Hola mundo"
    const isTrue = true;
    return(
        <div className="HolaMundo">
            <h1> {Hello} </h1>
            <h2>Curso esencial de React</h2>
            <img src="https://cdn.auth0.com/blog/react-js/react.png" alt="React"/>
            {isTrue ? <h4>esto es verdadero</h4> : <h5>Soy falso</h5>}
            {isTrue && <h5>soy verdadero otra validacion</h5>}
        </div>
    );
};

export default HolaMundo;